var conn = require('./../inc/db')
var express = require('express');
var reservations = require('../views/inc/reservations');
var router = express.Router();
var menus = require('../views/inc/menus');

/* GET home page. */
router.get('/', function (req, res, next) {
  menus.getMenus().then(results => {
    res.render('index', {
      title: 'Restaurante Saboroso',
      menus: results,
      isHome: true
    });
  });
});

router.get('/contacts', function (req, res, next) {
  res.render('contacts', {
    title: 'Contato - Restaurante Saboroso',
    background: 'images/img_bg_3.jpg',
    h1: 'Diga um oi',
  });
});

router.get('/menus', function (req, res, next) {
  menus.getMenus().then(results => {
    res.render('menus', {
      title: 'Menu - Restaurante Saboroso',
      background: 'images/img_bg_1.jpg',
      h1: 'Saboreie nosso menu!',
      menus: results
    });
  });
});

router.get('/reservations', function (req, res, next) {

  reservations.render(res, req);

});

router.post('/reservations', function (req, res, next) {
  if(!req.body.name) {
    reservations.render(res, req, 'Digite o nome');
  } else if(!req.body.email) {
    reservations.render(res, req, 'Digite email');
  }else if(!req.body.people) {
    reservations.render(res, req, 'Selecione o número de pessoas');
  }else if(!req.body.date) {
    reservations.render(res, req, 'Selecione a data');
  }else if(!req.body.time) {
    reservations.render(res, req, 'Selecione a hora');
  } else {
    reservations.save(req.body).then(res => {
      reservations.render(req, res, null, "Reserva realizada com sucesso !");
    }).catch(err => {
      reservations.render(req, res, err.message);
    })
  }
});

router.get('/services', function (req, res, next) {
  res.render('services', {
    title: 'Serviços - Restaurante Saboroso',
    background: 'images/img_bg_1.jpg',
    h1: 'É um prazer servir!'
  });
});

module.exports = router;
