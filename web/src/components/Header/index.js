import React from 'react'
import { Container } from './styles';

export default function Header() {
  return (
    <div>
        <Container>
            <h2>
              Uau Burguers
            </h2>
        </Container>
    </div>
  )
}
