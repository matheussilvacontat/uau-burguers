import React from 'react'
import './index.css';
import Logo from '../../images/logo.png'

function index() {
  return (
    <div className="">
      <header className="">
      <img
          src={Logo}
          className="logo"
          alt="Sample image"
        />
        <nav className="nav">
          <div className="nav-about">Sobre</div>
          <div className="nav-service">Serviços</div>
          <div className="nav-contact">Contato</div>
          <div className="nav-client">Clientes</div>
        </nav>
      </header>
    </div>
  )
}

export default index